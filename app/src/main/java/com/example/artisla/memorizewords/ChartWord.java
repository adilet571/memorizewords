package com.example.artisla.memorizewords;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class ChartWord extends ActionBarActivity {

    DataBaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        db = new DataBaseHelper(this);

        BarChart barChart = (BarChart)findViewById(R.id.chart);
        BarData barData = new BarData(getXAxisValues(),getDataSet());

        barChart.setData(barData);
        barChart.setDescription("My Progress");
        barChart.animateXY(2000, 2000);
        barChart.invalidate();


    //    textView.setText("Today: "+today+" Yesterday:  "+yesterday+"  ");
    }

    private ArrayList<BarDataSet> getDataSet(){
        ArrayList<BarDataSet> dataSets;


        ArrayList<BarEntry> values = new ArrayList<>();
        BarEntry value1 = new BarEntry(db.getTodayWords(),0);
        BarEntry value2 = new BarEntry(db.getYesterdayWords(),0);

        values.add(value1);
        values.add(value2);

        BarDataSet barDataSet = new BarDataSet(values,"Words");

        dataSets = new ArrayList<>();

        dataSets.add(barDataSet);

        return dataSets;
    }

    private ArrayList<String> getXAxisValues(){
        ArrayList<String> xValues = new ArrayList<>();

        xValues.add("Yesterday");
        xValues.add("Today");

        return xValues;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
