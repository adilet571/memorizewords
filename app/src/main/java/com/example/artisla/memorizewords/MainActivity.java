package com.example.artisla.memorizewords;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {


    Button new_word,list_word,chart_word;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new_word = (Button)findViewById(R.id.new_word);
        list_word = (Button)findViewById(R.id.list_word);
        chart_word = (Button)findViewById(R.id.chart_word);

        new_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick1(view);
            }
        });

        list_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick1(view);
            }
        });

        chart_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick1(view);
            }
        });


    }

    private void onClick1(View view){
        Intent intent;

        switch (view.getId()){
            case R.id.new_word:
                intent = new Intent(this,NewWord.class);
                startActivity(intent);
                break;
            case R.id.list_word:
                intent = new Intent(this,ListWord.class);
                startActivity(intent);
                break;
            case R.id.chart_word:
                intent = new Intent(this,ChartWord.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
