package com.example.artisla.memorizewords;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by artisla on 1/2/16.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "wordsLearner";
    private static final String TABLE_WORDS = "words";

    private static final String KEY_ID = "id";
    private static final String KEY_WORD = "word";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_CREATED_AT = "created_at";

    public DataBaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String create_words_table = "CREATE TABLE "+TABLE_WORDS+"("+KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+KEY_CREATED_AT+" DATE, "+KEY_WORD+" TEXT, "+KEY_DESCRIPTION+" TEXT )";
        db.execSQL(create_words_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_WORDS);
        onCreate(db);
    }

    public void addWord(Word word){
        SQLiteDatabase db = getWritableDatabase();

        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

        Log.d("Date",date);
        ContentValues contentValues =  new ContentValues();
        contentValues.put(KEY_WORD,word.getWord());
        contentValues.put(KEY_DESCRIPTION,word.getDescription());
        contentValues.put(KEY_CREATED_AT, date);
        Log.d("TIME", date);
        db.insert(TABLE_WORDS, null, contentValues);
        db.close();
    }

    public Word getWord(long id){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_WORDS, new String[]{KEY_ID, KEY_WORD, KEY_DESCRIPTION}, KEY_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Word word = new Word(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2));
        return word;

    }


    // Getting all words
    public List<Word> getAllWords(){
     List<Word> wordList = new ArrayList<Word>();

    String query = "SELECT * FROM "+TABLE_WORDS+" ORDER BY "+KEY_CREATED_AT+" DESC";
     //   String query = "SELECT * FROM "+TABLE_WORDS;
    SQLiteDatabase db = getWritableDatabase();
    Cursor cursor = db.rawQuery(query,null);

    if(cursor.moveToFirst()){
        do {
            Word word = new Word();
            word.setWord(cursor.getString(2));
            word.setDescription(cursor.getString(3));

            wordList.add(word);
        }while (cursor.moveToNext());
    }
        return wordList;
    }


    public float getTodayWords(){

        String query = "SELECT * FROM "+TABLE_WORDS+" WHERE "+KEY_CREATED_AT+" = '"+ new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())+"'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);


            return cursor.getCount();
    }

    public float getYesterdayWords(){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,-1);
        String query = "SELECT * FROM "+TABLE_WORDS+" WHERE "+KEY_CREATED_AT+" = '"+ simpleDateFormat.format(calendar.getTime())+" '";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        return cursor.getCount();
    }


}
