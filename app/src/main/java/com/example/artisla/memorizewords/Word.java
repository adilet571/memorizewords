package com.example.artisla.memorizewords;

/**
 * Created by artisla on 1/2/16.
 */
public class Word {
    long id;
    String word,description;

    public Word(){}

    public Word(long id,String word,String description){
        this.id = id;
        this.word = word;
        this.description = description;
    }

    public Word(String word,String description){
        this.word = word;
        this.description = description;
    }

    public long getId(){
        return this.id;
    }


    public String getWord() {
        return this.word;
    }
    public void setWord(String word){
        this.word = word;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
