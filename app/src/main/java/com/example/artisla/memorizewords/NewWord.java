package com.example.artisla.memorizewords;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;


public class NewWord extends ActionBarActivity {

    Button save;
    EditText wordET,descriptionET;
    DataBaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);
        db = new DataBaseHelper(this);
        wordET = (EditText)findViewById(R.id.word);
        descriptionET = (EditText)findViewById(R.id.description);

        save = (Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData(view);
            }
        });


    }

    private void saveData(View view){
        if( !(wordET.getText().toString().isEmpty()) && !(descriptionET.getText().toString().isEmpty())) {
            Word word = new Word(wordET.getText().toString() ,descriptionET.getText().toString());
            db.addWord(word);
            Toast.makeText(this,"Saved!",Toast.LENGTH_LONG).show();
            wordET.setText("");
            descriptionET.setText("");
        }
        else{
            Toast.makeText(this,"Please fill in the gaps!",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_word, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
