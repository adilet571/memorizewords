package com.example.artisla.memorizewords;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.EventLogTags;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class ListWord extends ActionBarActivity {

    private ArrayAdapter<String> arrayAdapter;
    private DataBaseHelper db;
    private ListView listView;
    private static List<Word> words;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_word);
        db = new DataBaseHelper(this);
        db.getReadableDatabase();
        words = db.getAllWords();
        List<String> wordList = new ArrayList<String>();

        for (Word word:words){
            wordList.add(word.getWord());
        }
        listView = (ListView)findViewById(R.id.wordLV);

        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,wordList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String description = words.get(i).getDescription();
                //Log.d("INFO",description);
                Intent intent = new Intent(ListWord.this, WordDescription.class);
                intent.putExtra("description",description);
                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_word, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
